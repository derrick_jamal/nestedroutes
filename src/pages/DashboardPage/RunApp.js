import React, { useState, useEffect, useMemo } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import TableContainer from './base-table';
import * as ReactBootstrap from 'react-bootstrap';
import { useHistory, NavLink } from "react-router-dom";
import styled from 'styled-components';
import "./tasksapp.css";


const options = {
  method: 'GET',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json;charset=UTF-8',
    'Authorization': 'Token 77fd0100284d3be96ef9991282f96821220cf903'
    // 'Authorization': 'Token ' + sessionStorage.getItem('token')
  },
};

const DownloadButton = styled.button`
    width: 100px;
    height: 30px;
    border-radius: 5px;
    border: solid 1px #0022ff;
    color: #0022ff;
    font-size: 12px;
`

const NavLinkTabs= styled.ul`
display: flex;
list-style: none;
margin-left: -40px !important;
`

const Link = styled(NavLink)`
text-decoration: none !important;
  border: solid 1px rgba(0, 34, 255, 0.3);
  border-radius: 5px;
  width: 105.5px;
  height: 40px;
  justify-content: space-evenly;
  display: flex;
  align-items: center;
`

const TaskRunApp = props => {
    const [completeness, setcompleteness] = useState([]);
    const [runs, setruns] = useState([]);
    const [loading, setloading] = useState(false);

    const fetchData = async () => {
      try{
        const url = 'http://51.158.103.243:8000/clusters?format=json';
        const res = await fetch(url, options).then(response => {
          return response.json();
        });
        setcompleteness(res["results"]);
      } catch (e) {
          console.log(e)
      }
  }
    useEffect(() => {
      fetchData();
    }, []);

    const filtered = completeness.filter(function(item){
        return item.name == props.match.params.value;
    })

    const fetchruns = async (data) => {
      try{
        const url = `http://51.158.103.243:8000/cluster/${data.id}/runs`;
        console.log('url', url)
        const res = await fetch(url, options).then(response => {
          return response.json();
        });
        setruns(res['results']);
        setloading(true);
      } catch (e) {
          console.log(e)
      }
    }
    useEffect(() => {
      fetchruns(filtered[0]);
    }, [filtered[0]]);

    const columns = useMemo(
      () => [
        {
          Header: 'Status',
          accessor: 'status',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Started',
          accessor: 'start_date',
          disableSortBy: true,
          disableFilters : true,
        },
        {
          Header: 'Duration',
          accessor: '',
          disableSortBy: true,
          disableFilters : true,
        },
        {
          Header: 'last Run At',
          accessor: 'end_date',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Total Requests',
          accessor: '',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Download',
          accessor: '',
          Cell: () => (
            <DownloadButton>
              Download
            </DownloadButton>
          ),
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        }
      ],
      []
    );

    const task = props.match.params.value
    return(
    <>
          <nav>
        <NavLinkTabs>
          <Link to={`/${task}/runs`} activeStyle={{ backgroundColor: '#0022ff', color:'#FFF' }}>
            Runs
          </Link>
          <Link to={`/${task}/input`} activeStyle={{ backgroundColor: '#0022ff', color: '#FFF' }}>
            Input
          </Link>
        </NavLinkTabs>
      </nav>
      <div className="completeness-table">
          {/* <p>table goes here</p> */}
        {loading ? (
          <TableContainer columns={columns} data={runs} />
        ):
        (
          <ReactBootstrap.Spinner animation="border"/>
        )}
      </div>
    </>
  );
}

export default TaskRunApp;