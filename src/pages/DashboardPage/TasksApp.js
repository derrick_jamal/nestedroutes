import React, { useState, useEffect, useMemo } from 'react';
import TableContainer from './base-table';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as ReactBootstrap from 'react-bootstrap';
import { useHistory, Link } from "react-router-dom";
import styled from 'styled-components';
import "./tasksapp.css";

const options = {
  method: 'GET',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json;charset=UTF-8',
    'Authorization': 'Token 77fd0100284d3be96ef9991282f96821220cf903'
    // 'Authorization': 'Token ' + sessionStorage.getItem('token')
  },
};

const CreateButton = styled.button`
  font-size: 14px;
  font-weight: 600;
  background: none;
  border :none;
  border-radius: 5px;
  width: 161px;
  height: 40px;
  background-color : #ff0000 !important;
  text-transform: none !important;
  color : #ffffff;
`

const TaskName = styled.div`
  color: #0022ff;
`


const TasksApp = props => {
    const [completeness, setcompleteness] = useState([]);
    const [runs, setruns] = useState([]);
    const [loading, setloading] = useState(false);


    const fetchData = async () => {
      try{
        const url = 'http://51.158.103.243:8000/clusters?format=json';
        const res = await fetch(url, options).then(response => {
          return response.json();
        });
        setcompleteness(res["results"]);
      } catch (e) {
          console.log(e)
      }
  }

    useEffect(() => {
      fetchData();
    }, []);

    const fetchRuns = async (data) => {
      let populatedData = [];
      try{
        for await (let row of data){
        const url = `http://51.158.103.243:8000/cluster/${row.id}/runs`;
        const res = await fetch(url, options).then(response => {
          return response.json();
        });
        console.log('resdata', res)
        row['rundata'] = res
        populatedData.push(row)
      }
        setruns(populatedData);
        setloading(true);
      } catch (e) {
          console.log(e)
      }
  }

  useEffect(() => {
    if (completeness.length === 0) return
    fetchRuns(completeness)
   },[completeness])
  //  console.log('data', completeness)
   console.log('runs', runs)
   console.log('runs', runs.rundata)


    const columns = useMemo(
      () => [
        {
          Header: 'Task Name',
          accessor: 'name',
          Cell:e =><TaskName>{e.value}</TaskName>,
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Square',
          accessor: d => `${d.module.name} ${d.function}`,
          disableSortBy: true,
          disableFilters : true,
        },
        {
          Header: 'Schedule',
          accessor: 'schedules',
          disableSortBy: true,
          disableFilters : true,
        },
        {
          Header: 'Runs',
          accessor: 'rundata.count',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Last Run At',
          accessor: 'rundata.results[0].end_date',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Last Run Status',
          accessor: 'rundata.results[0].status',
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        },
        {
          Header: 'Last Run Duration',
          accessor: 'rundata.results[0].end_date' == 'null' ? null: 
          Math.abs(new Date('rundata.results[0].end_date') - new Date('rundata.results[0].start_date'))
          ,
          disableSortBy: true,
          disableFilters : true,
          filter: 'equals',
        }
      ],
      []
    );

    const history = useHistory();

    const rowInfo = (rowobject) => {
      history.push(`/${rowobject.cells[0].value}/runs`)
        // console.log('rowinfo', rowobject.cells[0].value)
    }
  
  
    return(
    <>
      <CreateButton>
        Create new task
      </CreateButton>
      <div className="completeness-table">
          {/* <p>table goes here</p> */}
        {loading ? (
          <TableContainer rowInfo={rowInfo} columns={columns} data={runs} />
        ): 
        (
          <ReactBootstrap.Spinner animation="border"/>
        )}
      </div>
    </>
  );
}
export default TasksApp;