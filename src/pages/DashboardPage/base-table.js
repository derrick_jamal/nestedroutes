import React, { Fragment } from 'react';
import {
  useTable,
  useSortBy,
  useFilters,
  useExpanded,
  usePagination,
} from 'react-table';
import { Table, Row, Col, Button, Input, CustomInput } from 'reactstrap';
import styled from "styled-components";
import "./tasksapp.css";


const Wrapper = styled.div`
  width: 100%;
  background: #ffffff;
  box-shadow: 0 1px 3px 0 rgba(118, 112, 153, 0.13);
  border: 1px solid #ffffff;
  border-radius: 4px;
`


const Header = styled.thead`
  background : #ffffff;
  width: calc(100% - 17px);
`

const TableBody = styled.tbody`
  height: 305px;
  overflow-y: auto;
`

const BodyCell = styled.td`
  font-size: 13px;
  font-weight: 400;
  color: #211758;
`
const MatrixTable = styled(Table)`
    width:100%;
    background: #ffffff;
    border-collapse: collapse;
    border-spacing: 0;
`

const HeaderCell = styled.th`
  box-shadow: 0 1px 3px 0 rgba(118, 112, 153, 0.13);
  border-bottom: 1px solid white !important;
  margin-bottom: 1px;
  padding-top: 0px !important;
  padding-right: 2px !important;
  padding-bottom: 0px !important;
  padding-left: 10px !important;
  height: 32px;
  line-height: 32px;
  font-size:11px;
  font-weight: 600;
  opacity: 0.6;
  color: #211758;
`

const Pagination = styled.div`
  padding: 0px 10px;
  display:flex;
  justify-content: space-between;
`

const PaginationSectorOne = styled.div`
  display:flex;
  align-items: baseline;
`

const PaginationSectorTwo = styled.div`
  display:flex;
    align-items: baseline;
`

const Result = styled.p`
margin-right: 10px;
  font-size:12px;
  color: #b1b3c4;
`
const PageSizeInput = styled(CustomInput)`
  width: 66px !important;
  height: 30px !important;
  font-size: 12px !important;
  color: #211758 !important;
`
const DataCompute = styled.p`
margin-left: 10px;
  font-size: 12px;
  font-weight:600px !important;
  color: #211758;
`

const Previous = styled.p`
  font-size: 12px;
  color: #b1b3c4;
  cursor: default;
`

const Next = styled.p`
  font-size:  12px;
  color: #b1b3c4;
  margin-left:15px;
  cursor: default;
`

const PageList = styled.div`
  display: flex;
`

const Page = styled.li`
text-decoration: none;
display: flex;
justify-content: space-evenly;
align-items: center;
font-size: 14px;
color: #767099;
background-color: #f5f6fb;
border-radius: 2px;
width: 30px;
height: 30px;
margin-left: 15px;
`

const TableContainer = ({ columns, data, renderRowSubComponent, rowInfo }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    visibleColumns,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 10 },
    },
    useFilters,
    useSortBy,
    useExpanded,
    usePagination
  );

  const generateSortingIndicator = (column) => {
    return column.isSorted ? (column.isSortedDesc ? ' 🔽' : ' 🔼') : '';
  };

  const onChangeInSelect = (event) => {
    console.log('event value', event.target.value)
    setPageSize(Number(event.target.value));
  };
  const onChangeInInput = (event) => {
    console.log('targeted', event.target.value)
    const page = event.target.value ? Number(event.target.value) - 1 : 0;
    gotoPage(page);
  };

  console.log('number of rows', data.length)
  let pageNumbers = [];
  let minNumber = 1;
  let maxNumber = pageOptions.length
  let pageSizes = [];
  let maxSize =  Math.ceil(data.length / 10) * 10;
  let minSize = 10;

    for(let i = minNumber; i <= maxNumber; i += 1){
    pageNumbers.push(i);
}


  for(let i = minSize; i <= maxSize; i += 10){
    pageSizes.push(i);
}

  return (
    <Fragment>
      <Wrapper>
      <div>
      <MatrixTable hover {...getTableProps()}>
        <Header>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <HeaderCell {...column.getHeaderProps()}>
                  <div {...column.getSortByToggleProps()}>
                    {column.render('Header')}
                    {generateSortingIndicator(column)}
                  </div>
                  {/* <Filter column={column} /> */}
                </HeaderCell>
              ))}
            </tr>
          ))}
        </Header>
        <TableBody {...getTableBodyProps()}>
          {page.map((row) => {
            prepareRow(row);
            return (
              <Fragment key={row.getRowProps().key}>
                <tr {...row.getRowProps({onClick: () => rowInfo(row)} )}>
                  {row.cells.map((cell) => {
                    return (
                      <BodyCell {...cell.getCellProps()}>{cell.render('Cell')}</BodyCell>
                    );
                  })}
                </tr>
                {row.isExpanded && (
                  <tr>
                    <td colSpan={visibleColumns.length}>
                      {renderRowSubComponent(row)}
                    </td>
                  </tr>
                )}
              </Fragment>
            );
          })}
        </TableBody>
      </MatrixTable>
      </div>
      <Pagination>
        <PaginationSectorOne>
        <Result>Results</Result>
        <PageSizeInput
            type='select'
            value={pageSize}
            onChange={onChangeInSelect}
          >
            {pageSizes.map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                1-{pageSize}
              </option>
            ))}
          </PageSizeInput>
          <DataCompute>out of {data.length} tasks</DataCompute>
        </PaginationSectorOne>
        <PaginationSectorTwo>
          <Previous
            onClick={previousPage}
            disabled={!canPreviousPage}
          >
            {'Prev'}
          </Previous>
          <PageList>
          {pageNumbers.map((PageNumber) => (
              <Page onClick = {onChangeInInput} key= {PageNumber} value = {PageNumber} className = {pageIndex+1 === PageNumber ? 'linkactive': ''}>
                {PageNumber}
              </Page>
            ))}
          </PageList>
          <Next onClick={nextPage} disabled={!canNextPage}>
            {'Next'}
          </Next>
        </PaginationSectorTwo>
      </Pagination>
      </Wrapper>
    </Fragment>
  );
};

export default TableContainer;