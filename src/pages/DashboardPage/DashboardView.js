import React from "react";
import { Alert, Row, Button } from "antd";

import routes from "../../routes";

const DashboardView = props => {
  const goToSettingsPage = () => {
    props.history.push(routes.settings);
  };
  const goToStatisticsPage = () => {
    props.history.push(routes.statistics);
  };
  const goTablePage = () => {
    props.history.push(routes.table);
  };
  return (
    <div>
      {/* Title of page */}
      <Row
        type="flex"
        justify="center"
        style={{ margin: "120px 0px 32px 0px" }}
      >
        <Alert
          message="Dashboard splash view"
          description="call me dashboard"
          type="info"
          showIcon
        />
      </Row>

      {/* Navigation to nested routes */}
      <Row type="flex" justify="center">
        <Button style={{ marginRight: 50 }} onClick={goToSettingsPage}>
          To Settings
        </Button>
        <Button onClick={goToStatisticsPage}>To Statistics</Button>
        <Button onClick={goTablePage}>To Table</Button>
      </Row>
    </div>
  );
};

export default DashboardView;
