export default {
    // Roots
    home: "/",
    login: "/login",
    dashboard: "/dashboard",
    // Nested Dashboard pages
    table: '/dashboard/tasks',
    runs: '/:value/runs',
    statistics: "/dashboard/statistics",
    settings: "/dashboard/settings"
  };
  